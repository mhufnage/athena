#include "../TrigMessageSvc.h"
#include "../TrigMonTHistSvc.h"
#include "../HltEventLoopMgr.h"
#include "../HltAsyncEventLoopMgr.h"
#include "../HltROBDataProviderSvc.h"
#include "../TrigCOOLUpdateHelper.h"

DECLARE_COMPONENT( TrigMessageSvc )
DECLARE_COMPONENT( TrigMonTHistSvc )
DECLARE_COMPONENT( HltEventLoopMgr )
DECLARE_COMPONENT( HltAsyncEventLoopMgr )
DECLARE_COMPONENT( HltROBDataProviderSvc )
DECLARE_COMPONENT( TrigCOOLUpdateHelper )
