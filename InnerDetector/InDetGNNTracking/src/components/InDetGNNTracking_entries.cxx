#include "../SeedFitterTool.h"
#include "../SiGNNTrackFinderTool.h"
#include "../SiSPGNNTrackMaker.h"
#include "../GNNTrackReaderTool.h"
#include "../DumpObjects.h"

using namespace InDet;

DECLARE_COMPONENT( SeedFitterTool )
DECLARE_COMPONENT( SiGNNTrackFinderTool )
DECLARE_COMPONENT( GNNTrackReaderTool )
DECLARE_COMPONENT( SiSPGNNTrackMaker )
DECLARE_COMPONENT( DumpObjects )
